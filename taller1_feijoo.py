#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 14 11:36:23 2019

@author: martin
"""

#### Ejercicio 2 ####

# La función Resacón, para un n>=1, devuelve un número que es primo, se
# puede escribir como 2**n - 1 y en cuyos anteriores haya almenos n que sean
# primos y 2N-1s. Es decir, me devuelve el n-esimo numero primo y 2N1

def es2N1(x):
    import math
    z = math.log2(x+1)%1
    # Aca busco que x = 2**n - 1 y después pido que sea entero con el %1
    if z==0:
        return True
    else:
        return False
    
def esPrimo(x):
    y = abs(x)
    asd = []
    for i in range(1,y+1):
        asd.append(y%i==0)
    if sum(asd)==2:
        return True
    else:
        return False
    
def Resacon(n):
    numeros = []
    i = 0
    x = 1
    while i<n:
        if esPrimo(x)==True and es2N1(x)==True:
            i+=1
            numeros.append(x)
            x+=1
        else:
            x+=1
    return numeros[-1]